from datetime import datetime
import dominate
from dominate.tags import a, comment, h1, li, link, p, ul
from dominate.util import text
from feedgen.feed import FeedGenerator
import requests

apis = []
# maybe use named tuples instead of normal ones
apis.append(('PC', 'http://content.warframe.com/dynamic/worldState.php'))
apis.append(('XBOX1', 'http://content.xb1.warframe.com/dynamic/worldState.php'))
apis.append(('PS4', 'http://content.ps4.warframe.com/dynamic/worldState.php'))
apis.append(('Switch', 'http://content.swi.warframe.com/dynamic/worldState.php'))

baseurl = 'https://warframe-rss.gitlab.io/warframe-rss/'
favicon = 'https://n9e5v4d8.ssl.hwcdn.net/images/warframe/favicon.ico'


def convert_time(numberLong):
    # convert numberLong time to a more usable format
    # taken from https://stackoverflow.com/questions/9744775/how-to-convert-integer-timestamp-to-python-datetime#9744811
    unix_time = int(numberLong) / 1e3
    formated_time = datetime.utcfromtimestamp(unix_time).strftime('%Y-%m-%dT%H:%M:%S+00:00')
    return formated_time


def get_events(api):
    r = requests.get(api, timeout=(3.05, 27))
    events = r.json()['Events']
    # list is oldest first, I want newest first
    events.reverse()
    # reorganize around languages
    d = {}
    for event in events:
        # starting on 2019-12-17 sometime the URL is EventLiveUrl instead of Prop
        if event['Prop']:
            url = event['Prop']
        elif 'EventLiveUrl' in event:
            url = event['EventLiveUrl']
        else:
            url = 'https://playwarframe.com'
        for x in event['Messages']:
            time = convert_time(event['Date']['$date']['$numberLong'])
            if x['LanguageCode'] not in d:
                d[x['LanguageCode']] = []
            # should use named tuples instead of normal ones
            d[x['LanguageCode']].append((x['Message'], url, time))
    return d


def add_entries(items):
    for item in items:
        fe = fg.add_entry(order='append')
        fe.title(item[0])
        fe.id(item[1])
        fe.link(href=item[1])
        fe.contributor(name='Space Mom')
        fe.content('<a href="'+item[1]+'">'+item[0]+'</a>', type='html')
        fe.atom_entry(extensions=True)
        fe.updated(item[2])


def make_feeds(dataset, lang, platform):
    filename = platform+'_'+lang+'_atom.xml'
    fileurl = baseurl+filename
    global fg
    fg = FeedGenerator()
    # add metadata
    fg.logo(favicon)
    fg.icon(favicon)
    fg.link(href=baseurl, rel='alternate')
    fg.link(href=fileurl, rel='self')
    fg.id(fileurl)
    fg.title('Warframe in game news for '+platform)
    fg.contributor(name='Space Mom')
    fg.language(lang)
    # add news items
    add_entries(dataset[lang])
    # write feeds
    fg.atom_file(filename, pretty=True)


def make_index(langs, platforms):
    doc = dominate.document(title='RSS feeds for Warframe in game news')
    with doc.head:
        link(rel='shortcut icon', href=favicon)
    doc += h1('RSS feeds for Warframe in game news')
    para = p()
    with para:
        for plat in sorted(platforms):
            text(plat+':')
            list = ul()
            for l in sorted(langs):
                filename = plat+'_'+l+'_atom.xml'
                list += li(a(l, href=filename))
    doc += para
    doc += comment('created ' + datetime.utcnow().strftime("%Y-%m-%d %H:%M %z") + ' UTC')
    with open('index.html', 'w') as f:
        f.write(doc.render())


plat_list = []
for plat in apis:
    news = get_events(plat[1])
    plat_list.append(plat[0])
    for l in news.keys():
        make_feeds(news, l, plat[0])

make_index(list(news.keys()), plat_list)
